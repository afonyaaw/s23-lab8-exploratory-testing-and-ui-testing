const {Builder, By, until} = require('selenium-webdriver')
const assert = require('assert')

const PAGE_URL_TEST = 'https://www.quora.com/What-are-examples-of-automated-testing'

/**
 * The following code opens quora article page and checks if
 * after we click button read more, the text with count of views appeats
 * */
const start = async () => {
    let driver = await new Builder().forBrowser('chrome').build()
    await driver.get(PAGE_URL_TEST)
    const continueReadingButton = await driver
        .findElement(By
            .xpath('//*[@id="mainContent"]/div[3]/div[1]/div/div/div/div/div/div[2]/div/div[2]/div/div/div[2]/div[1]/div/div/div/span/div/div/div/button'))

    continueReadingButton.click()

    let viewsCountText = await driver.wait(until.elementLocated(By.xpath('//*[@id="mainContent"]/div[3]/div[1]/div/div/div/div/div/div[2]/div/div[3]/div/span/span[2]')),10000);

    let textDataParsed = await viewsCountText.getText()

    assert.equal(textDataParsed.split(' ')[1],'views')

    await driver.quit()
}

start()
